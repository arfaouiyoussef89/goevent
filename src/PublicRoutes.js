import React   from 'react';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {

  HomePage as HomePage,
  Icons as IconsView,
  SignUp as SignUpView,
  EventList as EventListView,

  SignIn as SignInView,
  NotFound as NotFoundView
} from './views';


export const PublicRoutes = () => {

   return  (
      <>


<RouteWithLayout
  component={HomePage}
  exact

  layout={MinimalLayout}
  path="/"
/>


<RouteWithLayout
component={EventListView}
exact
layout={MinimalLayout}
path="/public-events"
  />
  <RouteWithLayout
component={EventListView}
exact
layout={MainLayout}
path="/events"
  />

  <RouteWithLayout
component={IconsView}
exact
layout={MainLayout}
path="/icons"
  />





  <RouteWithLayout
component={HomePage}
exact
layout={MinimalLayout}
path="/HomePage"
  />
  <RouteWithLayout
component={SignUpView}
exact
layout={MinimalLayout}
path="/sign-up"
  />

  <RouteWithLayout
component={SignInView}
exact
layout={MinimalLayout}
path="/sign-in"
  />

  <RouteWithLayout
component={NotFoundView}
exact
layout={MinimalLayout}
path="/not-found"
  />
  </>);
        };

