import React, { useState } from 'react';
import { Switch, Redirect, BrowserRouter } from 'react-router-dom';

import { AdminRoutes } from './AdminRoutes';
import { PublicRoutes } from './PublicRoutes';
import { OtherRoutes } from './OtherRoutes';
import { AdministrationRoutes } from './AdminstrationRoutes';
import ProfileContextProvider from "./State/contexts/ProfileContext";


const Routes = () => {


  return (


<>
<ProfileContextProvider>
  <AdminRoutes/>
  <AdministrationRoutes/>
</ProfileContextProvider>

  <PublicRoutes/>

</>
      );

};

export default Routes;
