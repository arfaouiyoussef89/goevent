import React  from 'react';
import { Main as MainLayout } from './layouts';

import {
  universityList as UniversityListView,
  Account as AccountView,
  SchoolList as SchoolListView,
  EventList as EventListView,
  AddUniversity as AddUniversityView,
  AddSchool as  AddSchoolView,


  EditUniversity as EditUniversityView,
  ClubList as club,
} from './views';
import { PrivateRoute } from './components/RouteWithLayout/Private/PrivateRoutes';


export const AdministrationRoutes = () => {
  {
    return (
      <>

        <PrivateRoute
          component={AddSchoolView}
          exact
          layout={MainLayout}
          path="/administration/school/add-school"
        />
        <PrivateRoute
          component={UniversityListView}
          exact
          layout={MainLayout}
          path="/administration/university"
        />
        <PrivateRoute component={EditUniversityView}
                      exact
                      layout={MainLayout}
                      path={"/administration/university/:id/edit-university"}/>
        <PrivateRoute component={EditUniversityView}
                      exact
                      layout={MainLayout}
                      path={"/administration/university/:id/edit-school"}/>

        <PrivateRoute
          component={AddUniversityView}
          exact
          layout={MainLayout}
          path="/administration/university/add-university"
        />
        <PrivateRoute
          component={SchoolListView}
          exact

          layout={MainLayout}
          path="/administration/school"
        />
        <PrivateRoute
          component={club}
          exact
          layout={MainLayout}
          path="/administration/clubs"
        />

        <PrivateRoute
          component={EventListView}
          exact
          layout={MainLayout}
          path="/administration/events"
        />

        <PrivateRoute
          component={AccountView}
          exact
          layout={MainLayout}
          path="/administration/account"
        />
        <PrivateRoute
          component={AccountView}
          exact
          layout={MainLayout}
          path="/admin/settings"
        />


      </>);
  }
  ;
}
