import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ layout: Layout,component: Component, ...rest }) => (

  <Route
    {...rest} render={props => (

    localStorage.getItem('role')
      ? <Layout><Component {...props} /></Layout>
      : <Redirect to={{ pathname: '/sign-in', state: { from: props.location } }} />
  )} />
);


