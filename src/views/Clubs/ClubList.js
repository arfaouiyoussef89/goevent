import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import ClubContextProvider from '../../State/contexts/ClubContext';
import ClubComponents from './Club';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginBottom: theme.spacing(2)
  }
}));

export const ClubList = () => {
  const classes = useStyles();


  return (
    <div className={classes.root}>
      <ClubContextProvider>
      <ClubComponents className={classes.content}/>
      </ClubContextProvider>
      </div>
  );
};

export default ClubList;
