import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,

  Typography
} from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Grid from '@material-ui/core/Grid';
import { ClubContext } from '../../../State/contexts/ClubContext';
import SearchInput from '../../../components/SearchInput';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop:'20px'
  },
  row: {
    height: '42px',
    display: 'flex',
    marginBottom:theme.spacing(2),
    alignItems: 'center',
    marginTop: theme.spacing(1)
  }, wrapIcon: {
    verticalAlign: 'middle',

    display: 'inline-flex'},

  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  action:{
    display:"flex"
  }
}));

const clubName = undefined;
const ClubExpansion = props => {
  const classes = useStyles();

  const { className, ...rest } = props;
  const { state } =  useContext(ClubContext);

  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] =  React.useState(state);
  const handleChange = e => {
    setSearchTerm(e.target.value);
  };
  useEffect(() => {
    const results = (state.filter(res =>
      res.id.toLowerCase().includes(searchTerm)
    )) ;
    setSearchResults(results);
  }, [searchTerm,state]);





return(
  <div className={classes.root}>
    <div className={classes.row}>

      <SearchInput
        className={classes.searchInput}
        placeholder="Search school"
        value={searchTerm}
        onChange={handleChange}

      />
    </div>
    {searchResults.length === 0 ?<Grid xs={12} md={12} style={{textAlign:"center",height:"250px",marginTop:"15px"}} item ><Card >
        <img style={{height:"250px"}} src={require("../../../assets/img/not.jpg")}/></Card></Grid>:

    <div>
      {searchResults.map(item => (
        <div key={item.id}>
          <ExpansionPanel expanded={false}>

            <ExpansionPanelSummary

              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Grid container spacing={2}>

                <Grid item xs={11}>

                  <Typography variant="subtitle1" className={classes.wrapIcon}>
                    <img src={item.logo} width={"50px"} height={"50px"}  style={{borderRadius:'50%'}}/>
                    <p  style={{marginLeft:"5px",padding:"15px"}}> {item.clubName}</p>   <p  style={{marginLeft:"5px",padding:"15px"}}>  {item.email}</p>    <p  style={{marginLeft:"5px",padding:"15px"}}> {item.faculty}</p>   <p  style={{marginLeft:"5px",padding:"15px"}}> {item.domaine}</p>      </Typography></Grid>
                <Grid item xs={1}  style={{textAlign:"right",marginTop:"10px"}}><EditIcon color={"primary"} /> <Action id={item.id} action={deleteAction}> <DeleteIcon color={"error"}/></Action></Grid>

              </Grid>
            </ExpansionPanelSummary>

          </ExpansionPanel>


        </div>
      ))}
    </div>}
  </div>







);
};

function Action({ id, className, action, children }) {
  const {dispatch} = useContext(ClubContext);
  return (
    <a
      className={className}
      onClick={() => {
        dispatch(action(id));
      }}
    >
      {children}
    </a>
  );
}

export function deleteAction(taskId) {
  return {
    type: "REMOVE_EVENT",
    id: taskId
  };
}

ClubExpansion.propTypes = {
  className: PropTypes.string,
};

export default ClubExpansion;
