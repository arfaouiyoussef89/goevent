import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import { UsersToolbar } from './components';
import mockData from './data';
import UniversityExpansion from '../University/UniversityList/UniversityExpansion';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const EventList = () => {
  const classes = useStyles();

  const [users] = useState(mockData);

  return (
    <div className={classes.root}>
      <UsersToolbar />
      <div className={classes.content}>
        <UniversityExpansion users={users}  />
      </div>
    </div>
  );
};

export default EventList;
