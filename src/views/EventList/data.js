import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    name: 'J2EE Formation',
    address: {
      country: 'Tunis',
      state: 'Tunis',
      city: 'tn',
      street: ' Fulton Street'
    },
    email: 'Club Java',
    phone: '304-428-3097',
    avatarUrl: '/images/avatars/event.jpg',
  },{
    id: uuid(),
    name: 'Android Formation',
    address: {
      country: 'Tunis',
      state: 'Tunis',
      city: 'tn',
      street: ' Fulton Street'
    },
    email: 'Club Andorid ',
    phone: '304-428-3097',
    avatarUrl: '/images/avatars/event.jpg',
  },{
    id: uuid(),
    name: 'js Formation',
    address: {
      country: 'Tunis',
      state: 'Tunis',
      city: 'tn',
      street: ' Fulton Street'
    },
    email: 'Club js',
    phone: '304-428-3097',
    avatarUrl: '/images/avatars/event.jpg',
  },
];
