import React, { useContext, useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import { Updateadmin, Updateadministration } from '../../../../services/administration';
import { fetchInitial, ProfileContext } from '../../../../State/contexts/ProfileContext';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles(() => ({
  root: {}
}));

const AccountDetails = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const {state,dispatch}=useContext(ProfileContext);

  const [values, setValues] = useState(state);


  useEffect(()=>{
    setValues(state)
  },[state]);


  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };


  function updateUser() {
    let role=localStorage.getItem("role");
    switch (role) {
      case "administration":     Updateadministration(values).then(res=>{
        dispatch(fetchInitial(values))
      }); break;

      case "admin":  Updateadmin(values).then(res=>{
        dispatch(fetchInitial(values))
      }); break;

    }
    Updateadministration(values).then(res=>{
      dispatch(fetchInitial(values))
      toast.success("sucess ")
    },error=>{
      toast.error("failed");
    })
  }

  return (
    <Card
    >
      <form
        autoComplete="off"
        noValidate
      >
        <CardHeader
          subheader="The information can be edited"
          title="Profile"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <ToastContainer/>
              <TextField
                fullWidth
                helperText="Please specify the first name"
                label="Nom"
                margin="dense"
                name="nom"
                autoFocus={true}
                onChange={handleChange}
                value={values.nom}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                margin="dense"
                label={"Prenom"}
                name="prenom"
                onChange={handleChange}
                required
                autoFocus={true}
                value={values.prenom}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email Address"
                margin="dense"
                name="email"
                autoFocus
                onChange={handleChange}
                required
                value={values.email}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >

            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                helperText="Please specify the first name"
                label="password"
                margin="dense"
                name="password"
                autoFocus={true}
                onChange={handleChange}
                value={values.password}
                variant="outlined"
              />

            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >

            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button
            color="primary"
            variant="contained"
            onClick={updateUser}
          >
            Save details
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

AccountDetails.propTypes = {
  className: PropTypes.string
};

export default (AccountDetails);
