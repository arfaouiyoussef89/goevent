import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Typography,
  Divider,
  Button,
  LinearProgress
} from '@material-ui/core';
import { getAdministrationByid } from '../../Account';
import FileBase64 from '../../../../components/base64';
import { fetchInitial, ProfileContext } from '../../../../State/contexts/ProfileContext';
import { Updateadministration } from '../../../../services/administration';

const useStyles = makeStyles(theme => ({
  root: {},
  details: {
    display: 'flex'
  },
  avatar: {
    marginLeft: 'auto',
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0
  },
  progress: {
    marginTop: theme.spacing(2)
  },
  uploadButton: {
    marginRight: theme.spacing(2)
  }
}));

const AccountProfile = props => {
  const { className,...rest } = props;

  const classes = useStyles();
const {state,dispatch}=useContext(ProfileContext);
  const[user,setuser]=useState(state);

  const [logo,setlogo]=useState({});
useEffect(()=>{
},[state]);
  useEffect(()=>{
    dispatch(fetchInitial(user));


  },[user]);
  const  onChangeHandler=  async (event) => {


    let value = event;
    setlogo(value.base64.toString());
 setuser({...state,photo:value.base64.toString()});
Updateadministration(user);
  };
  return (
      <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <div className={classes.details}>
          <div>
            <Typography
              gutterBottom
              variant="h2"
            >
              {state.nom}  {state.prenom}
            </Typography>

            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1"
            >
              {state.email}            </Typography>
          </div>
          {state.photo !==null ?  <Avatar
            className={classes.avatar}
            src={state.photo}
          />: <Avatar
            className={classes.avatar}

            src={require("../../../../assets/img/add.png")}/> }

        </div>

      </CardContent>
      <Divider />
      <CardActions>

        <label htmlFor="image">

          <FileBase64 hidden={true}

                      multiple={ false }
                      onDone={onChangeHandler}
          />

          <a  type={"button"}>Upload photo</a>



        </label>
        <Button  onClick={()=>setlogo('')} variant="text">Remove picture</Button>
      </CardActions>
    </Card>
  );
};

AccountProfile.propTypes = {
  className: PropTypes.string
};

export default AccountProfile;
