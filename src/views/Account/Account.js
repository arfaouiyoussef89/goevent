import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { AccountProfile, AccountDetails } from './components';
import axios  from 'axios';
import ProfileContextProvider, { ProfileContext } from '../../State/contexts/ProfileContext';

export const getAdministrationByid = id =>{

  return  axios.get("/api/adminstration/GetAdministrationById/"+id)


};
export const getAdminByid = id =>{

  return  axios.get("/api/admin/GetAdminstarteurById/"+id)


};

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Account = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={4}
      >

        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <AccountProfile   />
        </Grid>
        <Grid
          item
          lg={8}
          md={6}
          xl={8}
          xs={12}
        >
          <AccountDetails  />
        </Grid>
      </Grid>
    </div>
  );
};

export default Account;
