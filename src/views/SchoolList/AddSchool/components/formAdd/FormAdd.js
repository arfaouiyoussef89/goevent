import React, { useContext, useEffect, useRef, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import '../../../../../assets/scss/style.css'
import {add} from "../../../../../services/school"
import { Controller, useForm } from 'react-hook-form';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import FileBase64 from '../../../../../components/base64';
import { UniversityContext } from '../../../../../State/contexts/UniversityContext';
import { withRouter } from 'react-router-dom';
const useStyles = makeStyles(() => ({
  root: {},
  img:{    overflow:" hidden",
width: "150px",
height: "150px",
borderRadius: "100px"
  }

}));


const FormAdd = props => {
  const { className,history, ...rest } = props;

  const classes = useStyles();
const[logo,setlogo]=useState(false);
  const { register, control,errors, handleSubmit, watch } = useForm({});
  const password = useRef({});
  password.current = watch("password", "");

  const dispatch=useContext(UniversityContext);







useEffect(()=>{
  console.log(logo)
},[logo]);


  const  onChangeHandler=  (event)=>{


let value=event;
    setlogo(value.base64.toString());
  };
  const onSubmit = values => {
    console.log(values);
    add(values).then(res=>{
      toast.success(" Success ");
      history.goBack();
    })
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >

      <form onSubmit={handleSubmit(onSubmit)}>
        <ToastContainer />


      <div>

        <CardHeader
          title="Add School"
        />

        </div>
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >

            <Grid
              item
              md={12}
              xs={12}
            >

<div >
  <div style={{margin:"auto", width:"200px",textAlign:"center"}}>
    <label htmlFor="image">

        <FileBase64 hidden={true}

                    multiple={ false }
                    onDone={onChangeHandler}
        />   { !logo ? (<a><img width={"150px"} height={"150px"} style={{borderRadius:"50%"}} src={require("../../../../../assets/img/add.png")}/></a>) : <a>
      <img width={"150px"} style={{borderRadius:"50%"}} height={"150px"} src={logo}/></a>  }
       <br/>


    </label>
  </div></div>
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <input name="logo" hidden
              value={logo}
              ref={register}/>
              <Controller
                as={<TextField  fullWidth

                                label="nom ecole"
                                margin="dense"
                                required
                                error=
                                  {errors.nom && errors.nom.message}
                                variant="outlined"/>}
                name="nom"
                control={control}
              />



            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >



              <Controller
                as={<TextField  fullWidth

                                label="Universty"
                                margin="dense"
                                required
                                variant="outlined"/>}
                name="universty"
                control={control}
              />

              {errors.universty && errors.universty.message}


            </Grid>






          </Grid>
        </CardContent>
        <Divider />
        <CardActions>

          <div >
          <Button
            color="primary"
            variant="contained"
type="submit"   >
            Save
          </Button>
</div>
        </CardActions>
      </form>
    </Card>
  );
};

FormAdd.propTypes = {
  className: PropTypes.string
};

export default withRouter(FormAdd);
