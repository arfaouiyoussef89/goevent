import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import '../../../assets/scss/style.css';
import { Controller, useForm } from 'react-hook-form';
 import axios from 'axios';


import { Button, Card, CardActions, CardContent, CardHeader, Divider, Grid, TextField } from '@material-ui/core';
import FileBase64 from '../../../components/base64';
import { toast } from 'react-toastify';
import { updateSchool } from "../../../services/school";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  },

  img:{    overflow:" hidden",
    width: "150px",
    height: "150px",
    borderRadius: "100px"
  }

}));

const EditSchool = props => {
  const { className, history,...rest } = props;


  const classes = useStyles();
  const[logo,setlogo]=useState('');

  const { handleSubmit,control, setValue,register, errors } = useForm();
  const { match } = props;
  let {id} = match.params;

var data="";
  useEffect(()=> {


  axios.get("/api/adminstration/getEcoleById/"+id,)
        .then(res => {


  data = res.data;
  setValue("nom", data.nom);
  setValue("universty", data.universty);
  setValue("id", data.id);
  setlogo(data.logo);
}
        );
        },[])       ;








  const  onChangeHandler=  (event)=>{


    setlogo(event.base64);
  };



  const onSubmit = values => {
console.log("form");
console.log(values)
    updateSchool(id,values).then(success=>{
      history.goBack();
    });
    toast.success('success')
//
  };

  return (

    <div className={classes.root}>
      <Grid
        container
        spacing={1}
      >

        <Grid
          item
          lg={12}
          md={12}
          xl={12}
          xs={12}
        >

    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >

      <form onSubmit={handleSubmit(onSubmit)}>


        <div>

          <CardHeader
            title="Edit University"
          />

        </div>
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >

            <Grid
              item
              md={12}
              xs={12}
            >

              <div >
                <div style={{margin:"auto", width:"200px",textAlign:"center"}}>
                  <label htmlFor="image">

                    <FileBase64 hidden={true}

                                multiple={ false }
                                onDone={onChangeHandler}
                    />   { !logo ? (<a><img width={"150px"} height={"150px"} style={{borderRadius:"50%"}} src={("../../../assets/img/add.png")}/></a>) : <a>
                    <img width={"150px"} style={{borderRadius:"50%"}} height={"150px"} src={logo}/></a>  }
                    <br/>


                  </label>
                </div></div>
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <input name="logo" hidden
                     value={logo}
                     ref={register}/>

              <input name="id" hidden
                     value={id}
                     ref={register}/>
       <Controller name={"nom"} control={control} as={    <TextField  fullWidth
                                label="nom université"
                                margin="dense"
                                required
                       name={"nom"}
                          ref={register}
                                variant="outlined"/>  }/>


              {errors.nom && errors.nom.message}


            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >




              <Controller name={"universty"} control={control} as={    <TextField  fullWidth
                                                                             label="universty"
                                                                             margin="dense"
                                                                             required
                                                                             name={"universty"}
                                                                             ref={register}
                                                                             variant="outlined"/>  }/>




            </Grid>

          </Grid>
        </CardContent>
        <Divider />
        <CardActions>

          <div >
            <Button
              color="primary"
              variant="contained"
              type="submit"   >
              Save
            </Button>
          </div>
        </CardActions>
      </form>
    </Card>

        </Grid>
      </Grid>
    </div>
  );
};

EditSchool.propTypes = {
  className: PropTypes.string
};

export default EditSchool;
