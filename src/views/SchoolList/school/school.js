import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';

import { Link as RouterLink } from 'react-router-dom';
import SearchInput from '../../../components/SearchInput';
import SchoolExpansion from '../SchoolExpansion';
import SchoolContextProvider, { SchoolContext } from '../../../State/contexts/SchoolContext';
const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  content: {
    marginTop: theme.spacing(2)
  }
  ,
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  }
}));



const School = props  => {
  const { className,...rest } = props;
  const classes = useStyles();



  return (
    <>
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.row}>
          <span className={classes.spacer} />
          <Button
            color="primary"
            variant="contained"
            component={RouterLink}
            to="/administration/school/add-school"
          >
            Ajouter Ecole</Button>
        </div>

      </div>
      <SchoolExpansion className={classes.content} />
    </>
  );
};

School.propTypes = {
  className: PropTypes.string
};

export default School;
