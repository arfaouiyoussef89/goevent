import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    name: 'School 1',
    address: {
      country: 'USA',
      state: 'West Virginia',
      city: 'Parkersburg',
      street: '2849 Fulton Street'
    },
    email: 'sponsor@gmail.com',
    phone: '24 588 666 ',
    avatarUrl: '/images/avatars/school.png',
  },
  {
    id: uuid(),
    name: 'School 2',
    address: {
      country: 'USA',
      state: 'West Virginia',
      city: 'Parkersburg',
      street: '2849 Fulton Street'
    },
    email: 'sponsor@gmail.com',
    phone: '24 588 666 ',
    avatarUrl: '/images/avatars/school.png',
  },
];
