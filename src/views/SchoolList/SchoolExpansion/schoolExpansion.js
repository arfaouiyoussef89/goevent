import React, { useContext, useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,

  Typography
} from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import SearchInput from '../../../components/SearchInput';
import { SchoolContext } from '../../../State/contexts/SchoolContext';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { deleteSchool } from '../../../services/school';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop:'20px'
  },  wrapIcon: {
    verticalAlign: 'middle',

    display: 'inline-flex'
  },
  row: {
    height: '42px',
    display: 'flex',
    marginBottom:theme.spacing(2),
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },

  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  action:{
    display:"flex"
  }
}));



const SchoolExpansion =  props => {
  const classes = useStyles();

  const { className, ...rest } = props;
  const { state,dispatch } =  useContext(SchoolContext);
const [open,setOpen]=useState();
  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] =  React.useState(state);
  const handleChange = e => {
    setSearchTerm(e.target.value);
  };
  const[id,setid]=useState();
  const handleClickOpen = (id) => {
    setOpen(true);
    setid(id);


  };
  const handleOk=async ()=>{
    await deleteSchool(id).then(res=>{
      dispatch(({type:"REMOVE_EVENT",id: id }));
      toast.success("Delete Success !");
    });
    setOpen(false)
  };

  const notify = () => toast("Wow so easy !");


  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const results = state.filter(res =>
      res.nom.toLowerCase().includes(searchTerm)
    );
    setSearchResults(results);
  }, [searchTerm,state]);

  return (
    <div className={classes.root}>
      <div className={classes.row}>
        <ToastContainer />

        <SearchInput
          className={classes.searchInput}
          placeholder="Recherche ecole"
          value={searchTerm}
          onChange={handleChange}

        />
      </div>
      {searchResults.length === 0 ?<Grid xs={12} md={12} style={{textAlign:"center",height:"250px",marginTop:"15px"}} item ><Card >
          <img style={{height:"250px"}} src={require("../../../assets/img/not.jpg")}/></Card></Grid>:


      <div>
        {
          searchResults.map(item => (
            <div key={item.id}>
              <ExpansionPanel expanded={false}>

                <ExpansionPanelSummary

                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Grid container spacing={2}>

                    <Grid item xs={11}>
                      <Typography variant="subtitle1" className={classes.wrapIcon}>
                        <img src={item.logo} width={"50px"} height={"50px"}  style={{borderRadius:'50%'}}/>
                        <p  style={{marginLeft:"5px",padding:"15px"}}> {item.nom}</p>       </Typography>
                    </Grid>
                    <Grid item xs={1}  style={{textAlign:"right",marginTop:"10px"}}>
                      <Link
                        to={`/admin/university/edit-school/${item.id}`}  ><EditIcon color={"primary"}/></Link> <a key={item.id} onClick={()=>handleClickOpen(item.id)}>
                      <DeleteIcon color={"error"}/></a></Grid>

                  </Grid>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>
                    {item.addresse}              </Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>


            </div>
          ))}
      </div>}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Supprimer université  !  ?"}</DialogTitle>
        <DialogContent>




        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Annuler
          </Button>
          <Button onClick={handleOk} color="secondary" autoFocus>
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    </div>


  );
};

function Action({ id, className, action, children }) {
  const {dispatch} = useContext(SchoolContext);

  function removeSchool() {
    deleteSchool(id).then(res=>{
      dispatch(action(id));
      alert("success")
    })
  }

  return (
    <a
      className={className}
      onClick={deleteSchool}
    >
      {children}
    </a>
  );
}


SchoolExpansion.propTypes = {
  className: PropTypes.string,
};

export default SchoolExpansion;
