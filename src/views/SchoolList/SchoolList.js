import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import School from './school';
import SchoolContextProvider from '../../State/contexts/SchoolContext';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  header:{
    marginBottom: theme.spacing(4)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const SchoolList = () => {
  const classes = useStyles();


  return (
    <div className={classes.root}>
      <div className={classes.content}>
      <SchoolContextProvider>
      <School  />
      </SchoolContextProvider>
    </div>
    </div>  );
};

export default SchoolList;
