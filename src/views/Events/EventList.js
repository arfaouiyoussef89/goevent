import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import EventContextProvider from '../../State/contexts/EventContext';
import uuid from 'uuid/v1';
import mockData from '../SchoolList/data';
import Event from './Event';
export const data= [
  {
    id: uuid(),
    name: 'School 1',
    address: {
      country: 'USA',
      state: 'West Virginia',
      city: 'Parkersburg',
      street: '2849 Fulton Street'
    },
    email: 'sponsor@gmail.com',
    phone: '24 588 666 ',
    avatarUrl: '/images/avatars/school.png',
  },
  {
    id: uuid(),
    name: 'School 5',
    address: {
      country: 'USA',
      state: 'West Virginia',
      city: 'Parkersburg',
      street: '2849 Fulton Street'
    },
    email: 'sponsor@gmail.com',
    phone: '24 588 666 ',
    avatarUrl: '/images/avatars/school.png',
  },
];

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginBottom: theme.spacing(2)
  }
}));

export const EventList = () => {
  const classes = useStyles();
  const [users] = useState(data);

  return (
    <div className={classes.root}>
      <EventContextProvider>
<Event />
      </EventContextProvider>
    </div>
  );
};

export default EventList;
