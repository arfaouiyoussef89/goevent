import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';

import { Link as RouterLink } from 'react-router-dom';
import { EventContext } from '../../../State/contexts/EventContext';
import SearchInput from '../../../components/SearchInput';
import EventExpansion from '../eventExpansion';
import TextField from "@material-ui/core/TextField";
const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  content: {
    marginTop: theme.spacing(2)
  }
  ,
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  }
}));


const Event = props  => {
  const { className,...rest } = props;
const {dispatch}=useContext(EventContext);
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] = React.useState([]);
  const handleChange = event => {
    setSearchTerm(event.target.value);
    console.log(event.target.value);
    dispatch({type:"FILTER_EVENT",search:event.target.value})
  };


//
  return (
    <>


      <div
        {...rest}
        className={clsx(classes.root, className)}
      >

        <div className={classes.row}>


        </div>

      </div>

      <EventExpansion className={classes.content} user={searchResults}/>

    </>
  );
};

Event.propTypes = {
  className: PropTypes.string
};

export default Event;
