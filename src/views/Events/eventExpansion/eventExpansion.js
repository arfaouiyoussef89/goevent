import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,

  Typography
} from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Grid from '@material-ui/core/Grid';
import { EventContext } from '../../../State/contexts/EventContext';
import { Link } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import SearchInput from "../../../components/SearchInput";
import TextField from "@material-ui/core/TextField";
import Chip from "@material-ui/core/Chip";



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop:'20px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  action:{
    display:"flex"
  },
  row: {
    height: '42px',
    display: 'flex',
    marginBottom:"15px",
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  content: {
    marginTop: theme.spacing(2)
  }
  ,
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  }

}));

const EventExpansion = props => {
  const classes = useStyles();

  const { className, ...rest } = props;
const event=useContext(EventContext);
  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] =  React.useState(event.event);
  const handleChange = e => {
    setSearchTerm(e.target.value);
  };
  useEffect(() => {
    const results = (event.event.filter(res =>
      res.name.toLowerCase().includes(searchTerm) ||   res.region.toLowerCase().includes(searchTerm)
      ||   res.startDate.toLowerCase().includes(startdate)
    )) ;
    setSearchResults(results);
  }, [searchTerm,startdate,event.event]);

  var startdate;
  return(
  <div className={classes.root}>

    <div className={classes.row}>
      <SearchInput
        className={classes.searchInput}
        placeholder="Search events by name or by region"
        value={searchTerm}
onChange={handleChange}
      />

      <form className={classes.container} noValidate>
        <TextField
          id="datetime-local"
          label="start date"
          type="datetime-local"
          className={classes.textField}
          value={startdate}
          onChange={handleChange}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </form>

    </div>
    {searchResults.length === 0 ?<Grid xs={12} md={12} style={{textAlign:"center",height:"250px",marginTop:"15px"}} item ><Card >
        <img style={{height:"250px"}} src={require("../../../assets/img/not.jpg")}/></Card></Grid>:

    <div>
      {searchResults.map(item => (
        <div key={item.id}>
          <ExpansionPanel >

            <ExpansionPanelSummary

              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Grid container spacing={2}>

                <Grid item xs={1}>
                  <Typography variant="subtitle1" className={classes.wrapIcon}>
                    <Avatar aria-label="recipe" className={classes.avatar}>
                      {item.name[0].toUpperCase()}
                    </Avatar>
                      </Typography>

                </Grid>
                <Grid item style={{marginTop:"2px"}} xs={3}>
                  <Typography variant="subtitle1" className={classes.wrapIcon}>
                 <p style={{padding:"5px"}}> {item.name}</p>
                  </Typography>

                </Grid>

                <Grid item xs={2}>
                  <Typography variant="subtitle1" className={classes.wrapIcon}>
                    <p style={{padding:"5px"}}> Region: {item.region}
                  </p>                  </Typography>

                </Grid>

              </Grid>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <p>  Description : {item.description} </p>

                <p>  startTime : {item.startTime} </p>
                <p>  endDate : {item.endDate} </p>
                <p>  endTime : {item.endTime} </p>
                <p>  validationDate : {item.validationDate} </p>
                <p>  region : {item.region} </p>
                <p>  location : {item.location} </p>

              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>


        </div>
      ))}
    </div>}
  </div>







);
};

function Action({ id, className, action, children }) {
  const {dispatch} = useContext(EventContext);
  return (
    <a
      className={className}
      onClick={() => {
        dispatch(action(id));
      }}
    >
      {children}
    </a>
  );
}

export function deleteAction(taskId) {
  return {
    type: "REMOVE_EVENT",
    id: taskId
  };
}

EventExpansion.propTypes = {
  className: PropTypes.string,
};

export default EventExpansion;
