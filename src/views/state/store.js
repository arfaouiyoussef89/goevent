import { createStore } from "redux";
import userDataReducer from "./reduce";

export default createStore(userDataReducer);
