import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import FormAdd from './components/formAdd';
import { withRouter } from 'react-router-dom';


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const AddUniversity = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={1}
      >

        <Grid
          item
          lg={12}
          md={12}
          xl={12}
          xs={12}
        >

          <FormAdd />
        </Grid>
      </Grid>
    </div>
  );
};

export default withRouter(AddUniversity);

