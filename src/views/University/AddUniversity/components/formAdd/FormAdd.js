import React, { useContext, useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import '../../../../../assets/scss/style.css';
import { add } from '../../../../../services/university';
import { Controller, useForm } from 'react-hook-form';


import { Button, Card, CardActions, CardContent, CardHeader, Divider, Grid, TextField } from '@material-ui/core';
import FileBase64 from '../../../../../components/base64';
import { UniversityContext } from '../../../../../State/contexts/UniversityContext';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  root: {},
  img:{    overflow:" hidden",
width: "150px",
height: "150px",
borderRadius: "100px"
  }

}));

const FormAdd = props => {
  const { className,history, ...rest } = props;

  const classes = useStyles();
  const [submit,setSubmit]=useState(false)
const[logo,setlogo]=useState(false);
  const { handleSubmit,control, register, errors } = useForm();
  const [values, setValues] = useState({
    nom:'' ,
    adresse:'' ,
    logo: '',
  });




  const  handleChange =  event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };


  var logos={};

useEffect(()=>{
  console.log(logo)
},[logo]);


  const  onChangeHandler=  (event)=>{


    setlogo(event.base64.toString());
  };
  const onSubmit = values => {
    setSubmit(true);
    add(values).then(res=>{
      toast.success(" Success ");
      setSubmit(false);
      history.goBack();
    },err=>{
      toast.error("Failed !");

    })
  };

  return (
    <Card
    >
      <ToastContainer />

      <form onSubmit={handleSubmit(onSubmit)}>


      <div>

        <CardHeader
          title="Add University"
        />

        </div>
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >

            <Grid
              item
              md={12}
              xs={12}
            >

<div >
  <div style={{margin:"auto", width:"200px",textAlign:"center"}}>
    <label htmlFor="image">

        <FileBase64 hidden={true}

                    multiple={ false }
                    onDone={onChangeHandler}
        />   { !logo ? (<a><img width={"150px"} height={"150px"} style={{borderRadius:"50%"}} src={require("../../../../../assets/img/add.png")}/></a>) : <a>
      <img width={"150px"} style={{borderRadius:"50%"}} height={"150px"} src={logo}/></a>  }
       <br/>


    </label>
  </div></div>
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <input name="logo" hidden
                     value={logo}
                     ref={register}/>
              <Controller
                as={<TextField  fullWidth

                                label="nom université"
                                margin="dense"
                                required
                                variant="outlined"/>}
                name="nom"
                control={control}
              />

              {errors.nom && errors.nom.message}


            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >



              <Controller
                as={<TextField  fullWidth

                                label="Adresse"
                                margin="dense"
                                required
                                variant="outlined"/>}
                name="adresse"
                control={control}
              />

              {errors.adresse && errors.adresse.message}


            </Grid>

          </Grid>
        </CardContent>
        <Divider />
        <CardActions>

          <div >
          <Button
            color="primary"
            variant="contained"
            disabled={submit}
type="submit"   >
            Save
          </Button>
</div>
        </CardActions>
      </form>
    </Card>
  );
};

FormAdd.propTypes = {
  className: PropTypes.string,
  history: PropTypes.object

};

export default withRouter(FormAdd);
