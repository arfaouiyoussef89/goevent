import React, { createContext, useContext, useReducer } from 'react';
import uuid from 'uuid/v1';

const CountContext = createContext(null);
export const initialState = [
  {
    id: uuid(),
    name: 'Isamm  ',
    address: {
      country: 'Manouba',
      state: '',
      city: 'tn',
      street: '2849 Fulton Street'
    }
  }
];
const ADD = "ADD";
const MARK = "MARK";
const DELETE = "DELETE";

export const reducer = (state, action) => {
  switch (action.type) {
    case "RESET":
      return initialState;
    case ADD:

      return [...state, action.payload.task];
    case "DELETE":
      return console.log(action.payload.taskId);

    default:
      return state;
  }
};

const CountContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, props.value || initialState);
  const value = { state, dispatch };
  return (
    <CountContext.Provider value={value}>{props.children}</CountContext.Provider>
  );
}

const CountContextConsumer = CountContext.Consumer;

export { CountContext, CountContextProvider, CountContextConsumer };
