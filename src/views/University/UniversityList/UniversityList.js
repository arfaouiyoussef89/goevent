import React, { useContext, useEffect, useReducer, useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import University from './University';
import UniversityContextProvider  from '../../../State/contexts/UniversityContext';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  header:{
    marginBottom: theme.spacing(4)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const UniversityList = () => {

  const classes = useStyles();

  {
  return (
    <div className={classes.root}>
      <div className={classes.content}>

<UniversityContextProvider >
        <University/>
</UniversityContextProvider>
</div>
      </div>
  );
}};

export default UniversityList;
