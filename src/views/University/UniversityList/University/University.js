import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';

import { Link as RouterLink } from 'react-router-dom';
import { fetchInitial, UniversityContext } from '../../../../State/contexts/UniversityContext';
import SearchInput from '../../../../components/SearchInput';
import UniversityExpansion from '../UniversityExpansion';
const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  content: {
    marginTop: theme.spacing(2)
  }
  ,
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  }
}));


const University = props  => {
  const { className,...rest } = props;
  const classes = useStyles();



//
  return (
    <>
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.row}>
          <span className={classes.spacer} />
          <Button
            color="primary"
            variant="contained"
            component={RouterLink}
            to="/admin/university/add-university"
          >
            Ajouter Université           </Button>
        </div>

      </div>
      <UniversityExpansion className={classes.content} />

    </>
  );
};

University.propTypes = {
  className: PropTypes.string
};

export default University;
