import React, { forwardRef } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { Container } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import { fontSize, color } from '@material-ui/system';

import { NavLink as RouterLink } from 'react-router-dom';
import EventList from '../EventList';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        GoEvent
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  paper: {
    position: 'absolute',
  },
  '@global': {
    body: {
      fontFamily: 'Audiowide',
      backgroundColor: theme.palette.common.white,
      backgroundImage: 'url(' + require("../../assets/img/home.png") + ')',
      backgroundSize: '89% 655px', overflow: 'hidden',
      backgroundRepeat: 'no-repeat',

    },
    ul: {
      margin: 0,
      padding: 0,
    },
    li: {
      listStyle: 'none',
    },
  },
  appBar: {
    backgroundColor: theme.palette.common.white,
    backgroundImage: 'url(' + require("../../assets/img/calendar.gif") + ')',
    backgroundSize: '8% 120%',
    //marginTop: theme.spacing(1),
    backgroundRepeat: 'no-repeat',
    flexGrow: 1,

  },
  toolbar: {
    flexWrap: 'wrap',
    //margin: theme.spacing(2, 1),
  },
  toolbarTitle: {
    flexGrow: 1,
    marginTop: theme.spacing(1.75),
    margin: theme.spacing(0, 6.5),
    fontFamily: 'Audiowide',
  },
  link: {
    
    margin: theme.spacing(0.5, 5.5),
  },
  heroContent: {
    //backgroundImage: 'url(' + require("../../assets/img/home.png") + ')',
    //backgroundSize: '1500px 1000px',
    //backgroundRepeat  : 'no-repeat',

    //flexGrow: 1,
    //padding: theme.spacing(10, 0, 46),
    fontFamily: 'Audiowide',


  },
  heroTitle: {
    fontFamily: 'Audiowide',
    color: "#671e2c",
    padding: theme.spacing(15, 58, 46),
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200],

  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(33),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
}));


function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
const tiers = [
];
const footers = [
  {
    title: 'Company',
    description: ['Team', 'History', 'Contact us', 'Locations'],
  },
  {
    title: 'Features',
    description: ['Cool stuff', 'Random feature', 'Team feature', 'Developer stuff', 'Another one'],
  },
  {
    title: 'Resources',
    description: ['Resource', 'Resource name', 'Another resource', 'Final resource'],
  },
  {
    title: 'Legal',
    description: ['Privacy policy', 'Terms of use'],
  },
];




function HomePage(props) {

  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [openr, setOpenr] = React.useState(false);

  const handleOpenr = () => {
    setOpenr(true);
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose2 = () => {
    setOpenr(false);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const CustomRouterLink = forwardRef((props, ref) => (
    <div
      ref={ref}
      style={{ flexGrow: 1 }}
    >
      <RouterLink {...props} />
    </div>
  ));
  
  return (
    <React.Fragment>
      <CssBaseline />
     
      {/* Hero unit */}
      <Container maxWidth="sm" hidden component="main" fontFamily="Audiowide" className={classes.heroContent}>
        <Typography component="h1" variant="h2" align="left" color='red' fontFamily="Audiowide" className={classes.heroTitle} color="#671e2c" gutterBottom>
          GoEvent
          <div style={{  justifyContent: 'left',position:"absolute",left:"680px", fontSize:'36px',fontFamily:"Arial" }}>
        <p>  Plateform of University Mangement Events</p> </div>
          </Typography>



      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {tiers.map(tier => (
            // Enterprise card is full width at sm breakpoint
            <Grid item key={tier.title} xs={12} sm={tier.title === 'Enterprise' ? 12 : 6} md={4}>
              <Card>
                <CardHeader
                  title={tier.title}
                  subheader={tier.subheader}
                  titleTypographyProps={{ align: 'center' }}
                  subheaderTypographyProps={{ align: 'center' }}
                  action={tier.title === 'Pro' ? <StarIcon /> : null}
                  className={classes.cardHeader}
                />
                <CardContent>
                  <div className={classes.cardPricing}>
                    <Typography component="h2" variant="h3" color="textPrimary">
                      ${tier.price}
                    </Typography>
                    <Typography variant="h6" color="textSecondary">
                      /mo
                      </Typography>
                  </div>
                  <ul>
                    {tier.description.map(line => (
                      <Typography component="li" variant="subtitle1" align="center" key={line}>
                        {line}
                      </Typography>
                    ))}
                  </ul>
                </CardContent>
                <CardActions>
                  <Button fullWidth variant={tier.buttonVariant} color="primary">
                    {tier.buttonText}
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>

    </React.Fragment>
  );
}

export default HomePage;
