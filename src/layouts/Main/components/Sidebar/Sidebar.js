import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import EventIcon from '@material-ui/icons/Event';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import SchoolIcon from '@material-ui/icons/School';import PeopleIcon from '@material-ui/icons/People';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import ImageIcon from '@material-ui/icons/Image';

import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SettingsIcon from '@material-ui/icons/Settings';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import GroupIcon from '@material-ui/icons/Group';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import { Profile, SidebarNav } from './components';
import StepIcon from '@material-ui/core/StepIcon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { Notifications } from "@material-ui/icons";
const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;
  // eslint-disable-next-line no-undef
  const user = {
    name: 'Sponsor',
    avatar: '/images/avatars/1.png',
    bio: 'Admin'
  };
  const classes = useStyles();
  const Administration = [


    {
      title: 'Clubs',
      href: '/administration/clubs',
      icon: <GroupIcon/>
    },

    {
      title: 'Events',
      href: '/administration/events',
      icon: <EventIcon />
    },



    {
      title: 'Account',
      href: '/administration/account',
      icon: <AccountBoxIcon />
    }

  ];

  const Admin = [

    {
      title: 'University',
      href: '/admin/university',
      icon: <AccountBalanceIcon/>
    },
    {
      title: 'School',
      href: '/admin/school',
      icon: <SchoolIcon />
    },
    {
      title: 'clubs',
      href: '/admin/clubs',
      icon: <PeopleIcon />
    },
    {
      title: 'signalements ',
      href: '/admin/signalements',
      icon: <Notifications/>
    },
    {
      title: 'événements',
      href: '/admin/events',
      icon: <EventIcon />
    },

    {
      title: 'Account',
      href: '/admin/account',
      icon:  <AccountBoxIcon />
    },

  ];
 let role=localStorage.getItem('role');
  let page;
  if(role.toLowerCase() ==='admin'){
   page=Admin;
}
else if(role.toLowerCase()==="administration"){
  page=Administration;
}
  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Profile user={user} />
        <Divider className={classes.divider} />
        <SidebarNav
          className={classes.nav}
          pages={page}
        />
      </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired

};

export default Sidebar;
