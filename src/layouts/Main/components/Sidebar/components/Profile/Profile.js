import React, { useContext, useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';
import { getAdministrationByid } from '../../../../../../views/Account/Account';
import { ProfileContext } from '../../../../../../State/contexts/ProfileContext';
import { universityReducer } from '../../../../../../State/reducers/UniversityReducer';
import { UniversityContext } from '../../../../../../State/contexts/UniversityContext';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 60,
    height: 60
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = props => {
  const { className, ...rest } = props;
  const { state } =  useContext(ProfileContext);


  const classes = useStyles();
  const [logo,setlogo]=useState({})
const [data,setdata]=useState({});
useEffect(()=>{

setdata(state);
}

,[state]);
  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      { state.photo  ?
        <Avatar
          alt="Person"
          className={classes.avatar}
          component={RouterLink}
          src={state.photo}
        />:  <Avatar
          alt="Person"
          className={classes.avatar}
          component={RouterLink}
          src={'/images/avatars/1.png'}
        />}
      <Typography
        className={classes.name}
        variant="h4"
      >
        {data.nom}
      </Typography>
      <Typography variant="body2">{data.email}</Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
