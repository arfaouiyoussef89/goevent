import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none',
    background:"white",
  },
  flexGrow: {
    flexGrow: 1
  },
  link: {
    
    margin: theme.spacing(0.5, 5.5),
  },

  signOutButton: {
    marginLeft: theme.spacing(1)
  }
}));

const Topbar = props => {
  const { className, onSidebarOpen, ...rest } = props;

  const classes = useStyles();
let photo =JSON.parse(localStorage.getItem("users"));

if(!photo){
  photo="/images/avatars/1.png"
}
else {
  photo = photo.photo;
}
let path;
let role=localStorage.getItem("role");
switch(role)
{
  case "admin": path="/admin/account";break;
  case "administration":path="/administration/account"
}
  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Toolbar>
       
       
        <RouterLink to="/">
          <img width="50px"
          height="50px"
            alt="Logo"
            src="/images/logos/calendar.gif"
          /><label style={{position:"relative",top:"-20px",fontFamily:"Audiowide"}}>Go events</label> 
        </RouterLink>
        <div className={classes.flexGrow} />
        <nav>
            <Link variant="button" color="textPrimary" href="#" className={classes.link}
            component={RouterLink}
                  to={"/HomePage"}

            >
              Home
              </Link>

            <Link variant="button" color="textPrimary" href="#" className={classes.link}
            
            component={RouterLink}
            to="/public-events"
            variant="h6"
            
            
            >
              Event
              </Link>
          {!localStorage.getItem("role")?    <Link variant="button" color="textPrimary" className={classes.link}
            
            component={RouterLink}
            to="/sign-in"
            variant="h6"
            >
            Login            </Link>:<Link to={path} component={RouterLink} variant={"h6"}><img src={photo}  style={{borderRadius:"50%",marginTop:"5px"}} width={"50px"} height={"50px"}/></Link>}
             


          </nav>

      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

export default Topbar;
