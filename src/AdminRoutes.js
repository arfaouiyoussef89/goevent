import React  from 'react';
import { Main as MainLayout } from './layouts';

import {
  updateSchool as UpdateSchoolView,
  universityList as UnivrListView,
  Account as AccountView,
  SchoolList as SchoolListView,
  EventList as EventListView,
  AddUniversity as AddUniversityView,
EditUniversity as EditUniversityView,
  ClubList as club,
} from './views';
import { PrivateRoute } from './components/RouteWithLayout/Private/PrivateRoutes';


export const AdminRoutes = () => {
  {
    return (
      <>

        <PrivateRoute
          component={UnivrListView}
          exact
          layout={MainLayout}
          path="/admin/university"
        />
        <PrivateRoute component={UpdateSchoolView}
                      exact
                      layout={MainLayout}
                      path={"/admin/university/edit-school/:id"}/>

        <PrivateRoute component={EditUniversityView}
                    exact
                    layout={MainLayout}
                    path={"/admin/university/edit-university/:id"}/>
<PrivateRoute component={EventListView}
              exact
              layout={MainLayout}
              path={"/admin/signalements"}/>
        <PrivateRoute
          component={AddUniversityView}
          exact
          layout={MainLayout}
          path="/admin/university/add-university"
        />
        <PrivateRoute
          component={SchoolListView}
          exact

          layout={MainLayout}
          path="/admin/school"
        />
        <PrivateRoute
          component={club}
          exact
          layout={MainLayout}
          path="/admin/clubs"
        />

        <PrivateRoute
          component={EventListView}
          exact
          layout={MainLayout}
          path="/admin/events"
        />

        <PrivateRoute
          component={AccountView}
          exact
          layout={MainLayout}
          path="/admin/account"
        />
        <PrivateRoute
          component={AccountView}
          exact
          layout={MainLayout}
          path="/admin/settings"
        />


      </>);
  }
  ;
}
