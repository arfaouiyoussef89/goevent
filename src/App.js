import React, { Component, useReducer } from 'react';
import {  Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Chart } from 'react-chartjs-2';
import { ThemeProvider } from '@material-ui/styles';
import validate from 'validate.js';
         import { Switch } from 'react-router-dom';
import { chartjs } from './helpers';
import theme from './theme';
import 'react-perfect-scrollbar/dist/css/styles.css';
import './assets/scss/index.scss';
import validators from './common/validators';
import Routes from './Routes';
import {  initialState, reducer } from './views/University/UniversityList/CounterContext';
import EventContextProvider from './State/contexts/EventContext';
import AuthContextProvider from './State/contexts/AuthContext';
import ProfileContextProvider from './State/contexts/ProfileContext';

const browserHistory = createBrowserHistory();

Chart.helpers.extend(Chart.elements.Rectangle.prototype, {
  draw: chartjs.draw
});

validate.validators = {
  ...validate.validators,
  ...validators
};
const  user={
  name:'youssef',
  lastname:'arfaoui',
  age:26
};

export default function App() {
    return (


      <ThemeProvider theme={theme}>

        <AuthContextProvider>

<EventContextProvider>
  <Router history={browserHistory}>

  <Switch>


         <Routes />

  </Switch>
      </Router>

</EventContextProvider>
        </AuthContextProvider>
      </ThemeProvider>

  );

}
