import { Promise } from 'q';

export function add(data) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify(data) )
  };

  return fetch('/api/adminstration/createUniversty/', requestOptions)


}
export function getById(id) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify({id:id}) )
  };

  return fetch('/api/administration/getUniverstyById/'+id, requestOptions)


}
export function update(id,univ) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify(univ) )
  };

  return ( fetch('/api/adminstration/updateuniversty/'+id, requestOptions))


}
export  const deleteUniv = id=>{
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify({id:id} ))};
return fetch('/api/adminstration/deleteuniversty/'+id, requestOptions)


  };

