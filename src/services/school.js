import { Promise } from 'q';

export function add(data) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify(data) )
  };

  return fetch('/api/adminstration/createEcole/', requestOptions)


}
export function getById(id) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify({id:id}) )
  };

  return fetch('/api/adminstration/get/'+id, requestOptions)


}
export function updateSchool(id,data) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify(data) )
  };

  return Promise.all( fetch('/api/adminstration/updateEcole/'+id, requestOptions))


}
export  const deleteSchool = id=>{
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    body: (JSON.stringify({id:id} ))};
  return fetch('/api/adminstration/deleteEcole/'+id, requestOptions)


};

