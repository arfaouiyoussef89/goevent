import * as uuid from 'uuid';


export const clubReducer=(state,action)=>{
    switch(action.type) {
      case "INITIAL":
        return  [...action.data];
      case "REMOVE_EVENT":
        return state.filter(event => action.id !== event.id );

      case "FILTER_EVENT":

        return state.filter(event => event.name.toLowerCase().includes(action.search.toLowerCase()) );


      default:
    return state



}




};
