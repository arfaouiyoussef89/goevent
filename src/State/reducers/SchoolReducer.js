import * as uuid from 'uuid';


export const schoolReducer=(state,action)=>{
    switch(action.type) {
      case "INITIAL":
        return  [...action.data];

  case "ADD_EVENT":
    return[...state,{
      title:action.event.name,
      author:action.event.author,
      name: action.event.name,
      email: action.event.email,
      phone: action.event.phone,
      avatarUrl:action.event.avatarUrl,
      id:uuid()
    }];
      case "REMOVE_EVENT":
        return state.filter(event => action.id !== event.id );

      case "FILTER_EVENT":

        return state.filter(event => event.name.toLowerCase().includes(action.search.toLowerCase()) );


      default:
    return state



}




};
