import * as uuid from 'uuid';
import { mydata } from '../contexts/UniversityContext';



export const universityReducer=(state,action)=>{
    switch(action.type) {
      case "FETCH_SONGS_REQUEST":
        return {
          ...state,
          isFetching: true,
          hasError: false
        };
      case "INITIAL":

        return  [...action.data];
        case "FETCH_SONGS_SUCCESS":
        return {
          ...state,
          isFetching: false,
          songs: action.payload
        };
      case "FETCH_SONGS_FAILURE":
        return {
          ...state,
          hasError: true,
          isFetching: false
        };

      case "ADD_UNIV":
    return[...state,{
      id:action.name,
      nom:action.author,
      logo: action.name,
      adresse: action.adresse,
    }];
      case "REMOVE":
        return state.filter(event => action.id !== event.id );

      case "FILTER_EVENT":

        return state.filter(event => event.title.toLowerCase().search(action.search.toLowerCase())!== -1);
      case "CLEAR":
        return mydata;


      default:
    return state



}




};
