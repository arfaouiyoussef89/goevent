


export const authReducer=(state,action)=>{
  switch(action.type) {
    case "LOGIN":
      localStorage.setItem("users", JSON.stringify(action.payload));
      localStorage.setItem("role", (action.payload.role));

      return {
        ...state,
        user: action.payload,
        role:action.payload.role
      }; break;
    case "LOGOUT":
      return {
        ...state,
        isAuthenticated: false,
        user: null
      };
    default:
      return state;
  }




};
