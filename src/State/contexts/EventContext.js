import React, { createContext, useEffect, useReducer } from 'react';
import { eventReducer } from '../reducers/EventReducer';


import uuid from 'uuid/v1';
import axios from 'axios';
import { fetchInitial } from './UniversityContext';



export const EventContext=createContext();

export const  EventContextProvider=(props)=> {
  const [event, dispatch] = useReducer(eventReducer, []);

  useEffect(   () => {

    axios.get("/api/adminstration/eventlist/")
      .then(data => {
        data=data.data;
        dispatch(fetchInitial(data));
      });
  }, []);





return(
    <EventContext.Provider value={{event , dispatch}}>
      {props.children}
    </EventContext.Provider>

  );


};
export default EventContextProvider;
