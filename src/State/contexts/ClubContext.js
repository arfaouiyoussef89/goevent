import React, { createContext, useEffect, useReducer } from 'react';
import { clubReducer } from '../reducers/ClubReducer';
import { fetchInitial } from './SchoolContext';



export const ClubContext=createContext();

export const  ClubContextProvider=(props)=>{
  const [state,dispatch]=useReducer(clubReducer, []);
  useEffect(()=>{

    fetch("/api/adminstration/clublist/")
      .then(response =>  response.json())
      .then(data => {
        dispatch(fetchInitial(data));
        console.log(data)
      });
  },[]);
  return(
    <ClubContext.Provider value={{state , dispatch}}>
      {props.children}
    </ClubContext.Provider>

  );


};
export default ClubContextProvider;
