import React, { createContext, useEffect, useReducer } from 'react';


import uuid from 'uuid/v1';
import { universityReducer } from '../reducers/UniversityReducer';
import { schoolReducer } from '../reducers/SchoolReducer';

const initialState = {
  songs: [],
  isFetching: false,
  hasError: false,
};

export const SchoolContext=createContext();
export const fetchInitial = data => ({
  type: "INITIAL",
  data
});

export const deleteUniv = id => ({
  type: "DELETE",
  id:id
});
export var mydata;
export const  SchoolContextProvider=  (props) => {
  const [state, dispatch] = useReducer(schoolReducer, []);
  useEffect(  () => {

    fetch("/api/adminstration/ecolelist/")
      .then(response =>  response.json())
      .then(data => {
        dispatch(fetchInitial(data));
      });
  }, []);


  return (
    <SchoolContext.Provider value={{ state, dispatch }}>
      {props.children}
    </SchoolContext.Provider>

  );


};
export default SchoolContextProvider;
