import React, { createContext, useEffect, useReducer } from 'react';
import { authReducer } from '../reducers/AuthReducer';

export const AuthContext=createContext();

export const  AuthContextProvider=(props)=>{
  const [event,dispatch]=useReducer(authReducer,{});
  useEffect(()=>{

    localStorage.setItem("user",JSON.stringify(event));
  },[event]);
  return(
    <AuthContext.Provider value={{event , dispatch}}>
      {props.children}
    </AuthContext.Provider>

  );


};
export default AuthContextProvider;
