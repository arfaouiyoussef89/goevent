import React, { createContext, useEffect, useReducer } from 'react';


import { universityReducer } from '../reducers/UniversityReducer';
import axios from 'axios';



export const UniversityContext=createContext();
export const fetchInitial = data => ({
  type: "INITIAL",
  data
});

export const deleteUniv = id => ({
  type: "DELETE",
  id:id
});
export var mydata;
export const  UniversityContextProvider=  (props) => {
  const [state, dispatch] = useReducer(universityReducer, []);
     useEffect(   () => {

    axios.get("/api/adminstration/universtylist/")
      .then(data => {
        data=data.data;
        dispatch(fetchInitial(data));
      });
  }, []);


  return (
    <UniversityContext.Provider value={{ state, dispatch }}>
      {props.children}
    </UniversityContext.Provider>

  );


};
export default UniversityContextProvider;
