import React, { createContext, useEffect, useReducer } from 'react';


import { ProfileReducer } from '../reducers/ProfileReducer';
import { getAdminByid, getAdministrationByid } from '../../views/Account/Account';



export const ProfileContext=createContext(null);
export const fetchInitial = data => ({
  type: "INITIAL",
  data
});
export const changePhoto = (data,photo) => ({
  type: "CHANGE",
  data,
  photo
});

export const updateProfile = data => ({
  type: "DELETE",
  data:data
});
export var mydata;
export const  ProfileContextProvider=  (props) => {
  const [state, dispatch] = useReducer(ProfileReducer, {});
    let datas=localStorage.getItem('users');
  let role=localStorage.getItem('role');
  let id;
if(role)
   id = JSON.parse(datas).id;
  let data = "";

  useEffect(() => {
      switch (role) {
        case "administration":
          getAdministrationByid(id).then(id).then(res => {
            data = res.data;
            dispatch(fetchInitial(data))

          });
          break;
        case "admin":
          getAdminByid(id).then(res => {
            data = res.data;
            dispatch(fetchInitial(data))
          });
          break;
      }


  }, [datas]);


  return (
    <ProfileContext.Provider value={{ state, dispatch }}>
      {props.children}
    </ProfileContext.Provider>

  );


};
export default ProfileContextProvider;
